package br.com.Conductor.Front.End.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.Conductor.Front.End.Support.Selenium;

public class PesquisarClientePage {
	private static WebDriver driver;
	private static WebDriverWait wait;
	
	static {
		driver = Selenium.getDriver();
		wait = new WebDriverWait(driver, 15);
	}
	
	public PesquisarClientePage(){
		this.driver = driver;
	}
	
	public void pesquisarCliente(String nome, String data){
		driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > a > i")).click(); //botao QA
		driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(1) > a")).click(); //botao Clientes	
		driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(1) > ul > li:nth-child(2) > a")).click(); //botao Listar Clientes
		
		driver.findElement(By.name("j_idt17")).sendKeys(nome); //Campo Nome
		driver.findElement(By.id("calendario_input")).sendKeys(data);; // Calendário para pesquisa
		driver.findElement(By.name("j_idt20")).click(); //botao Pesquisar
	}
	
	public void verificarDadosCliente(){
		driver.findElement(By.cssSelector("#formListarCliente > div > div > table > tbody > tr.success > td.text-center > a.btn.btn-sm.btn-primary")).click(); //botão Verificar dados do cliente
	}
	
	public void limparBase(){
		driver.findElement(By.name("j_idt22")).click(); //botão Limpar base
		driver.findElement(By.cssSelector("#alertMessage > strong")).isDisplayed(); //mensagem Base Limpa com Sucesso
	}
	
	public boolean listarClientes(){
		return driver.findElement(By.cssSelector("#formListarCliente > div > div > table")).isDisplayed();
	}
}
