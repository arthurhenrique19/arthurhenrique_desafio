package br.com.Conductor.Front.End.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.Conductor.Front.End.Support.Selenium;
import br.com.Conductor.Front.End.Util.Utils;

public class HomePage {
	private static WebDriver driver;
	private static WebDriverWait wait;
	
	static{
		driver = Selenium.getDriver();
		wait = new WebDriverWait(driver, 15);
	}
	
	public HomePage(){
		this.driver = driver;
	}
	
	public void pesquisarClientes(){
		try{
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > a > i")).click(); //botao QA
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(1) > a")).click(); //botao Clientes	
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(1) > ul > li:nth-child(2) > a")).click(); //botao Listar Clientes
		} catch (Exception e){
			Utils.tirarPrintDaTela("pesquisar cliente");
		}
	}
	
	public void cadastrarClientes(){
		try{
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > a > i")).click(); //botao QA
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(1) > a")).click(); //botao Clientes	
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(1) > ul > li:nth-child(1) > a")).click(); //botão Cadastrar Clientes
		} catch (Exception e){
			Utils.tirarPrintDaTela("cadastrar cliente");
		}
	}
	
	public void pesquisarTransacao(){
		try{
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > a > i")).click(); //botao QA
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(2) > a")).click(); //botão Transações
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(2) > ul > li:nth-child(2) > a")).click(); //botão Listar Transações
		} catch (Exception e){
			Utils.tirarPrintDaTela("pesquisar vendas");
		}
	}
	
	public void cadastrarTransacao(){
		try{
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > a > i")).click(); //botao QA
			driver.findElement(By.cssSelector("#left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(2) > a")).click(); //botão Transações
			driver.findElement(By.cssSelector(" #left-panel > nav > ul:nth-child(2) > li > ul > li:nth-child(2) > ul > li:nth-child(1) > a")).click(); //botão Cadastrar Transações
	} catch (Exception e){
		Utils.tirarPrintDaTela("cadastrar vendas");
		}
	}
	
	public void sair(){
		driver.findElement(By.cssSelector("#header > div.pull-right > form > input.btn.btn-primary")).click(); //botão Sair
	}
}
